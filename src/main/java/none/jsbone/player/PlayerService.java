package none.jsbone.player;

import java.util.List;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NonNull;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public List<Player> getAllPlayers() {
        return Lists.newArrayList(playerRepository.findAll());
    }

	public Player getPlayer(@NonNull String username) {
        return playerRepository.findById(username).orElse(null);
	}

	public void addPlayer(@NonNull Player player) {
        playerRepository.save(player);
	}

	public void updatePlayer(@NonNull String username, @NonNull Player player) {
        player.setUsername(username);
        playerRepository.save(player);
	}

	public void deletePlayer(String username) {
        playerRepository.deleteById(username);
	}
}
