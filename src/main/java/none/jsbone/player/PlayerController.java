package none.jsbone.player;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class PlayerController {
    @Autowired
    PlayerService playerservice;

    @RequestMapping("/players")
    public List<Player> getAllPlayers() {
        return playerservice.getAllPlayers();
    }

    @RequestMapping("/players/{username}")
    public Player getPlayer(@PathVariable String username){
        return playerservice.getPlayer(username);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/players")
    public void addPlayer(@RequestBody Player player) {
        playerservice.addPlayer(player);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/players/{username}")
    public void updatePlayer(@PathVariable("username") String username, @RequestBody Player player) {
        playerservice.updatePlayer(username, player);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/players/{username}")
    public void deletePlayer(@PathVariable String username) {
        playerservice.deletePlayer(username);
    }
}

