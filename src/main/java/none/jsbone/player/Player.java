package none.jsbone.player;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Entity
public class Player {
    @Getter
    @Setter
    @Id
    private String username;
    @Getter
    @Setter
    private String displayname;
    @Getter
    @Setter
    private String email;
}
