package none.jsbone;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class JSBOne {

    static public void main(String... args){
        System.out.println(String.format("Args: %s", Arrays.toString(args)));
        SpringApplication.run(JSBOne.class, args);
    }
}
